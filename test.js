/** Unit testing the puzzles.
 * @copyright 2022
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

import test from 'ava'
import { puzzles } from '@slugbugblue/trax-puzzles'
import { Trax } from '@slugbugblue/trax'

test('no duplicates', (t) => {
  /** @type {Record<string, string>} */
  const moves = {}
  for (const puzzle of puzzles) {
    const { notation } = new Trax(puzzle.game, puzzle.notation)
    t.falsy(moves[notation], `Puzzle ${puzzle.id} has a duplicate notation`)
    moves[notation] = puzzle.id
  }
})

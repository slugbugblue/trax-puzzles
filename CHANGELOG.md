# @slugbugblue/trax

## 0.5.0 - 2023-03-24

- Pull puzzles out into its own repository

## 0.4.0 - 2023-02-11

- Add license information for puzzle sources
- More puzzles from Martin M. S. Pedersen -- bringing the total to 100+

## 0.3.0 - 2023-02-05

- More puzzles:

  - Martin M. S. Pedersen contributed eleven new puzzles

## 0.2.0 - 2022-12-14

- Puzzle improvements:

  - Adjust puzzle category levels for harder puzzles
  - Add multiple hints when the puzzle can be solved multiple ways

## 0.1.0 - 2022-11-25

- Add puzzles:

  - From the collection of
    [Donald Bailey](http://traxgame.com/games_puzzles.php)
  - And some even easier "tutorial" puzzles

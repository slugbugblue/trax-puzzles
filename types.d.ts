/** Type information.
 * @copyright 2022-2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

/** A puzzle. */
type Puzzle = {
  id: string
  src: string
  game: TraxVariant
  notation: string
  icon: string
  level: number
  max: number
  player: number
  title?: string
  desc?: string
  hint?: string
  hints?: string[]
}

/** The source of a puzzle. */
type PuzzleSource = {
  name: string
  url?: string
  copyright?: string
  license?: string
  licenseUrl?: string
}
